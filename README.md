# README #

### What is this repository for? ###

* Demo examples used during [Vert.x](http://vertx.io/) Event Bus tech talk by [Vladimir Parfenov](https://bitbucket.org/vparfenov/)
* Find Vert.x Learning Materials [here](http://vertx.io/materials/)
* 1.0.1

### How do I get set up? ###

* Install [Gradle plugin](http://marketplace.eclipse.org/content/buildship-gradle-integration) into [Eclipse](http://www.eclipse.org). Make sure you are using latest [Eclipse Oxygen setup](https://www.eclipse.org/downloads/eclipse-packages/).
* Most likely you can use some other IDE but we never tried, e.g. it may require changes in build.gradle files to make them your IDE friendly.
* Clone this and dependencies repos into your workspace directory (e.g. with [Tortoise Git](https://tortoisegit.org/download/)). The dependencies are:
	* [vertxservices](https://bitbucket.org/vpteammi/vertxservices) � vert.x integration
	* [microservices](https://bitbucket.org/vpteammi/microservices) � our reactive wrapper
	* [sm-javax](https://bitbucket.org/vpteammi/sm-javax) � my open source java utilities
* Import all projects into Eclipse as Gradle projects (Gradle plugin should add that option to Eclipse "Import..." menu)
* You can look into [the presentation and the recording](https://ptccloud.sharepoint.com/teams/ISG-RnD-Employee-Dev-Team/Videos/Forms/Video/videoplayerpage.aspx?ID=347&FolderCTID=0x0120D520A8080087529BA28081F34E9B32398685E9FEB1&List=7940cf7f-7f4b-4d44-a869-d4b6b676eff9&RootFolder=%2Fteams%2FISG-RnD-Employee-Dev-Team%2FVideos%2FTech%20Talks%20vert%2Ex%20%26%20EventBus%2FAdditional%20Content&RecSrc=%2Fteams%2FISG-RnD-Employee-Dev-Team%2FVideos%2FTech%20Talks%20vert%2Ex%20%26%20EventBus&InitialTabId=Ribbon%2ERead&VisibilityContext=WSSTabPersistence) to understand the combination of client and service applications to run
* Find the books I mentioned uploaded into [the presentation](https://ptccloud.sharepoint.com/teams/ISG-RnD-Employee-Dev-Team/Videos/Forms/Video/videoplayerpage.aspx?ID=347&FolderCTID=0x0120D520A8080087529BA28081F34E9B32398685E9FEB1&List=7940cf7f-7f4b-4d44-a869-d4b6b676eff9&RootFolder=%2Fteams%2FISG-RnD-Employee-Dev-Team%2FVideos%2FTech%20Talks%20vert%2Ex%20%26%20EventBus%2FAdditional%20Content&RecSrc=%2Fteams%2FISG-RnD-Employee-Dev-Team%2FVideos%2FTech%20Talks%20vert%2Ex%20%26%20EventBus&InitialTabId=Ribbon%2ERead&VisibilityContext=WSSTabPersistence)
* You can also combine different test clients and servers in your own way
* Ask your questions

### Contribution guidelines ###

* Not expected at this point but any suggestions are welcome

### Who do I talk to? ###

* [vparfenov@ptc.com](mailto:vparfenov@ptc.com)
* [ddementiev@ptc.com](mailto:ddementiev@ptc.com)