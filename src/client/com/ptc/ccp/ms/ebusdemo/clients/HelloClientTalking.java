package com.ptc.ccp.ms.ebusdemo.clients;

import com.ptc.ccp.ms.ebusdemo.Logger;
import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI;
import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI.Message;

public class HelloClientTalking extends HelloClient
{
	public static void main (String[] args)
	{
		int repeats = Integer.parseInt (args.length > 0 ? args[0] : "1");
		reactiveFramework.connectToServerRx (HelloAPI.class)
				.subscribe (api -> observeRangeAndShutdown (repeats, 3000,
						idx -> api.heyRxResponse (new Message ("Talking Client #" + idx))
								.flatMap (response -> response.api (HelloAPI.class).heyRxResponse (response.value ()))
								.flatMap (response -> response.api (HelloAPI.class).heyRx (response.value ()))
								.doOnError (failure -> Logger.error (failure)),
						(idx, message) -> Logger.log ("Response: " + message.text)));
		Logger.log ("Talking Client started");
	}
}
