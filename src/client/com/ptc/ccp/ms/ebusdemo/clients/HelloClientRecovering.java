package com.ptc.ccp.ms.ebusdemo.clients;

import java.util.concurrent.TimeUnit;

import com.ptc.ccp.ms.ebusdemo.Logger;
import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI;
import com.sm.javax.langx.Strings;

public class HelloClientRecovering extends HelloClient
{
	public static void main (String[] args)
	{
		reactiveFramework.connectToServerRx (HelloAPI.class)
				.subscribe (api -> observeRangeAndShutdown (1000, 500,
						idx -> api.heyRx ("Recovering Client").timeout (3, TimeUnit.SECONDS).retry (3).onErrorReturn (
								ex -> "Recovered from " + Strings.noEmptyOr (ex.getLocalizedMessage (), ex)),
						(idx, message) -> Logger.log ("Response: " + message)));
		Logger.log ("Recovering Client started");
	}
}
