package com.ptc.ccp.ms.ebusdemo.clients;

import java.util.Random;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.ptc.ccp.ms.ebusdemo.calculator.Calculator;
import com.ptc.ccp.vertxservices.reactive.EbusReactiveConnector;

import io.reactivex.Single;

public class CalcClient
{

	public static void printError (Throwable err, boolean stack)
	{
		System.out.println ("Error: " + err.getMessage ());
		if (stack) {
			err.printStackTrace ();
		}
	}

	public static void expressionTest () throws InterruptedException
	{
		Single<Calculator> svc1 = EbusReactiveConnector.connectToServerRx ("ebus-calculator", Calculator.class);
		Single<Calculator> svc2 = EbusReactiveConnector.connectToServerRx ("ebus-calculator", Calculator.class);
		Single<Calculator> svc3 = EbusReactiveConnector.connectToServerRx ("ebus-calculator", Calculator.class);
		ScheduledThreadPoolExecutor pool = new ScheduledThreadPoolExecutor (4);
		Single.zip (svc1, svc2, svc3, (c1, c2, c3) -> {
			Random rand = new Random ();
			TotalStats stats = new TotalStats (300, 3);
			boolean countRetries = false;
			if (!countRetries) {
				stats.generator ().subscribe (v -> {
					pool.schedule ( () -> {
						System.out.println ("Executing " + v + " @ " + Thread.currentThread ().getId ());
						Single.zip (
								c2.addRx (11, 22).timeout (15, TimeUnit.SECONDS)
										.retry ( (ival, ex) -> stats.canRetry ("add", v, ival, ex)),
								c3.multiplyRx (3, 4).timeout (15, TimeUnit.SECONDS)
										.retry ( (ival, ex) -> stats.canRetry ("mult", v, ival, ex)),
								(o1, o2) -> c1.subtractRx (o1, o2).timeout (15, TimeUnit.SECONDS)
										.retry ( (ival, ex) -> stats.canRetry ("sub", v, ival, ex)))
								.flatMap (s -> s).retry ( (ival, ex) -> stats.canRetry ("expr", v, ival, ex))
								.doOnError (ex -> {
									stats.failure (v);
									// ex.printStackTrace ();
									System.err.println ("FAILED " + v + " as " + ex.getMessage ());
								}).compose (s -> s).subscribe (r -> {
									System.out.println (v + ": (11+22)-(3*4)=" + r);
									stats.success (v);
								});
					}, 50 * (10 + rand.nextInt (100)), TimeUnit.MILLISECONDS);
				});
			}
			else {
				stats.generator ().subscribe (v -> {
					pool.schedule ( () -> {
						System.out.println ("Executing " + v + " @ " + Thread.currentThread ().getId ());
						Single.zip (c2.addRx (11, 22).retry (2).compose (op -> {
							stats.retry (v);
							return op;
						}), c3.multiplyRx (3, 4).retry (2).compose (op -> {
							stats.retry (v);
							return op;
						}), (o1, o2) -> c1.subtractRx (o1, o2).retry (2).compose (op -> {
							stats.retry (v);
							return op;
						})).flatMap (s -> s).retry (2).doOnError (ex -> {
							stats.failure (v);
							ex.printStackTrace ();
						}).subscribe (r -> {
							System.out.println ("(11+22)-(3*4)=" + r);
							stats.success (v);
						});
					}, v * (10 + rand.nextInt (50)), TimeUnit.MILLISECONDS);
				});
			}
			stats.subscribe (stats::finalStatistics);
			return Boolean.TRUE;
		}).doOnError (ex -> ex.printStackTrace ()).subscribe (nothing -> {
			svc1.subscribe (s -> s.release ());
			svc2.subscribe (s -> s.release ());
			svc3.subscribe (s -> s.release ());
		});
		System.out.println ("Started process!");
	}

	public static void main (String[] args)
	{
		try {
			expressionTest ();
		}
		catch (Throwable e) {
			printError (e, false);
		}
	}
}
