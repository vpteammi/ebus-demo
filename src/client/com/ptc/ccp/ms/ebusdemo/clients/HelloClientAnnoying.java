package com.ptc.ccp.ms.ebusdemo.clients;

import com.ptc.ccp.ms.ebusdemo.Logger;
import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI;

public class HelloClientAnnoying extends HelloClient
{
	public static void main (String[] args)
	{
		reactiveFramework.connectToServerRx (HelloAPI.class)
				.subscribe (api -> observeRangeAndShutdown (1000, 500,
						idx -> api.heyRx ("Annoying Client").doOnError (failure -> Logger.error (failure)),
						(idx, message) -> Logger.log ("Response: " + message)));
		Logger.log ("Annoying Client started");
	}
}
