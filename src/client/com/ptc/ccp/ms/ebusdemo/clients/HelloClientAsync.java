package com.ptc.ccp.ms.ebusdemo.clients;

import com.ptc.ccp.ms.ebusdemo.Logger;
import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI;

public class HelloClientAsync extends HelloClient
{
	public static void main (String[] args)
	{
		reactiveFramework.connectToServerRx (HelloAPI.class).flatMap (api -> api.heyRx ("Async Client"))
				.doOnError (failure -> Logger.error (failure)).doFinally ( () -> reactiveFramework.shutdown ())
				.subscribe (message -> Logger.log ("Response: " + message));
		Logger.log ("Async Client started");
	}
}
