package com.ptc.ccp.ms.ebusdemo.clients;

import com.ptc.ccp.ms.ebusdemo.Logger;
import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI;

import io.reactivex.Single;

public class HelloClientComplex extends HelloClient
{
	public static void main (String[] args)
	{
		Single.zip (reactiveFramework.connectToServerRx ("first-names-service", HelloAPI.class),
				reactiveFramework.connectToServerRx ("last-names-service", HelloAPI.class),
				reactiveFramework.connectToServerRx (HelloAPI.class),
				(firstNameApi, lastNameApi,
						helloApi) -> observeRangeAndShutdown (1000, 500,
								idx -> Single
										.zip (firstNameApi.heyRx ("").onErrorReturn (ex -> "Calvin"),
												lastNameApi.heyRx ("").onErrorReturn (ex -> "Klein"),
												(firstName, lastName) -> helloApi
														.heyRx (idx + ") " + firstName + " " + lastName))
										.flatMap (s -> s).onErrorReturn (ex -> "Sorry Martha Stewart!"),
								(idx, message) -> Logger.log ("Response: " + message)))
				.doOnError (ex -> {
					Logger.error (ex);
					reactiveFramework.shutdown ();
				}).subscribe ();
		Logger.log ("Complex Client started");
	}
}
