package com.ptc.ccp.ms.ebusdemo.clients;

import java.util.concurrent.TimeUnit;

import com.ptc.ccp.ms.ebusdemo.utils.VariableInterval;
import com.sm.javax.utilx.TimeUtils;

public class RxJavaTests
{
	public static void main (String[] args)
	{
		System.out.println ("Starting: " + TimeUtils.toLogShortTimeFormat (TimeUtils.getNow ()));
		VariableInterval.observable (100, 1000, TimeUnit.MILLISECONDS).blockingSubscribe (
				v -> System.out.println (v + ": " + TimeUtils.toLogShortTimeFormat (TimeUtils.getNow ())));
	}
}
