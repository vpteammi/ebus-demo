package com.ptc.ccp.ms.ebusdemo.clients;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI;
import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI.Message;
import com.ptc.ccp.vertxservices.reactive.EbusReactiveConnector;
import com.ptc.microservices.ServiceException;

import io.reactivex.Observable;

public class HelloClientMisc
{

	public static void syncClient ()
	{
		try {
			HelloAPI api = EbusReactiveConnector.connectToServer (HelloAPI.class).get ();
			System.out.println ("Response: " + api.hey ("Vladimir"));
		}
		catch (ServiceException | InterruptedException | ExecutionException e) {
			e.printStackTrace ();
		}
	}

	public static void callbackClient ()
	{
		try {
			EbusReactiveConnector.connectToServer (HelloAPI.class, api -> {
				api.hey (new HelloAPI.Message ("Olga"), response -> {
					if (response.succeeded ()) {
						System.out.println ("Response: " + response.value ().text);
					}
					else {
						System.err.println ("Error: " + response.cause ().getMessage ());
					}
				});
			}, failure -> {
				failure.printStackTrace ();
			});
		}
		catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace ();
		}
	}

	public static void futureClient ()
	{
		EbusReactiveConnector.connectToServer (HelloAPI.class).thenCompose (api -> api.heyAsync ("Maria"))
				.whenComplete ( (message, failure) -> {
					if (failure != null)
						failure.printStackTrace ();
					else
						System.out.println ("Response: " + message);
				});
	}

	public static void rxClient ()
	{
		rxClient (1);
	}

	public static void rxClient (int repeats)
	{
		EbusReactiveConnector.connectToServerRx (HelloAPI.class).subscribe (api -> {
			Observable.intervalRange (0, repeats, 0, 3, TimeUnit.SECONDS)
					.subscribe (v -> api.heyRx ("Buddy").doOnError (failure -> failure.printStackTrace ())
							.subscribe (message -> System.out.println ("Response: " + message)));
		});
	}

	public static void talkingClient ()
	{
		EbusReactiveConnector.connectToServerRx (HelloAPI.class)
				.flatMap (api -> api.heyRxResponse (new Message ("Fantomas"))
						.flatMap (response -> api.reply (response).heyRxResponse (response.value ())))
				// .flatMap (response -> response.api (HelloAPI.class).heyRx
				// (response.value ()))
				.doOnError (failure -> failure.printStackTrace ())
				.subscribe (message -> System.out.println ("Response: " + message.value ().text));
	}

	public static void annoyingClient (String name, int attempts)
	{
		EbusReactiveConnector.connectToServerRx (HelloAPI.class).subscribe (api -> {
			Observable.range (0, attempts).subscribeOn (api.scheduler ()).subscribe (idx -> {
				api.heyRx (name + "#" + idx).doOnError (failure -> failure.printStackTrace ())
						.subscribe (message -> System.out.println ("Response: " + message));
			});
		});
	}

	public static void testClients ()
	{
		syncClient ();
		callbackClient ();
		futureClient ();
		rxClient ();
		annoyingClient ("Scooby", 6);
	}

	public static void main (String[] args)
	{
		// rxClient ();
		talkingClient ();
		// testClients ();
	}
}
