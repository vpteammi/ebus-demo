package com.ptc.ccp.ms.ebusdemo.clients;

import java.util.concurrent.ExecutionException;

import com.ptc.ccp.ms.ebusdemo.Logger;
import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI;
import com.ptc.microservices.ServiceException;

public class HelloClientSync extends HelloClient
{
	public static void main (String[] args)
	{
		try {
			Logger.log ("Sync Client started");
			HelloAPI api = reactiveFramework.connectToServer (HelloAPI.class).get ();
			Logger.log ("Response: " + api.hey ("Sync Client"));
			reactiveFramework.shutdown ();
		}
		catch (ServiceException | InterruptedException | ExecutionException e) {
			Logger.error (e);
		}
	}
}
