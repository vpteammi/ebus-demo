package com.ptc.ccp.ms.ebusdemo.clients;

import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.sm.javax.langx.Objects;

import io.vertx.core.impl.ConcurrentHashSet;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

class TotalStats
{
	static class TaskStats
	{
		Date started;
		long totalTime;
		boolean succeded;
		int retryCount;
	}

	int size;
	ConcurrentHashSet<Integer> tasks = new ConcurrentHashSet<> ();
	ConcurrentHashSet<Integer> failures = new ConcurrentHashSet<> ();
	ConcurrentHashSet<Integer> successes = new ConcurrentHashSet<> ();
	ConcurrentHashSet<Long> threads = new ConcurrentHashSet<> ();
	ConcurrentHashMap<Integer, Integer> retries = new ConcurrentHashMap<> ();
	ConcurrentHashMap<String, Integer> retriesByType = new ConcurrentHashMap<> ();
	CompletableFuture<Void> completionWatcher;
	Date started;
	int opCount;
	int retryCount;
	int retryMax;

	public TotalStats (int size, int opCount)
	{
		this.size = size;
		this.opCount = opCount;
	}

	public void consume (int id)
	{
		threads.add (Thread.currentThread ().getId ());
		tasks.remove (id);
		printStatistics ("Processed " + id);
		if (tasks.size () == 0) {
			completionWatcher.complete (null);
		}
	}

	public synchronized boolean canRetry (String op, int id, int retryCount, Throwable ex)
	{
		retriesByType.put (op, Objects.noNull (retriesByType.get (op), 0) + 1);
		Integer retryRecord = Objects.noNull (retries.get (id), 0);
		if (retryCount < retryMax) {
			retry (id);
			System.err.println ("Retrying '" + op + "' for " + id + " after " + ex + ", retries=" + retryCount);
			retries.put (id, retryRecord + 1);
			return true;
		}
		else {
			System.err.println ("Retries for " + id + " reached " + retryMax);
			return false;
		}
	}

	public synchronized void retry (int id)
	{
		retryCount++;
	}

	public synchronized void success (int id)
	{
		successes.add (id);
		consume (id);
	}

	public synchronized void failure (int id)
	{
		failures.add (id);
		consume (id);
	}

	public Observable<Integer> generator ()
	{
		tasks.clear ();
		completionWatcher = new CompletableFuture<Void> ();
		started = new Date ();
		retryCount = 0; // -size * opCount;
		retryMax = 3;
		return Observable.range (0, size).subscribeOn (Schedulers.computation ()).map (id -> {
			tasks.add (id);
			return id; // Observable.just(id).delay(100,
						// TimeUnit.MILLISECONDS);
		});
	}

	public void finalStatistics ()
	{
		String retryInfo = retriesByType.entrySet ().stream ().map (e -> e.getKey () + "=" + e.getValue ())
				.collect (Collectors.joining (", ", " [", "]"));
		printStatistics ("Done in " + (new Date ().getTime () - started.getTime ()) + " msecs (with " + retryCount
				+ " retries" + retryInfo + ")");
	}

	public void printStatistics (String title)
	{
		System.out.println (title + ": threads=" + threads.size () + ", success=" + successes.size () + ", fail="
				+ failures.size () + ", queue=" + tasks.size ());
	}

	public void subscribe (Runnable onComplete)
	{
		Single.fromFuture (completionWatcher).subscribeOn (Schedulers.computation ())
				.subscribe (x -> onComplete.run ());
	}
}
