package com.ptc.ccp.ms.ebusdemo.clients;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import com.ptc.microservices.reactive.IReactiveConnector;
import com.ptc.microservices.reactive.IReactiveFramework;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;

/*
 * Just a set of static helper shortcuts
 */
public class HelloClient
{
	public static IReactiveFramework reactiveFramework = IReactiveFramework.getDefault ();
	
	public static <T> CompletableFuture<T> subscribeToFuture (Single<T> single, Consumer<? super T> onNext)
	{
		return IReactiveConnector.subscribe (single, onNext);
	}

	public static <T> CompletableFuture<T> subscribeToFuture (Single<T> single)
	{
		return IReactiveConnector.subscribe (single);
	}

	public static <T> Disposable waitAndShutdown (Observable<CompletableFuture<T>> observable)
	{
		return reactiveFramework.waitAndShutdown (observable);
	}

	public static <T> Disposable observeRangeAndShutdown (long count, long periodMSecs, Function<Long,Single<T>> emitter,
			BiConsumer<Long, ? super T> onNext)
	{
		return waitAndShutdown (Observable.intervalRange (0, count, 0, periodMSecs, TimeUnit.MILLISECONDS)
				.map (v -> subscribeToFuture (emitter.apply (v), item -> onNext.accept (v, item))));
	}
}
