package com.ptc.ccp.ms.ebusdemo.utils;

import com.sm.javax.utilx.CommandLineArguments;

public interface ArgUtils
{
	public static String getName (String[] args, String defaultName)
	{
		return CommandLineArguments.parse (args).getArgument ("name", defaultName);
	}
	public static boolean hasArg (String[] args, String argName)
	{
		return CommandLineArguments.parse (args).hasArgument (argName);
	}
}
