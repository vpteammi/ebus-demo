package com.ptc.ccp.ms.ebusdemo.utils;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import com.ptc.ccp.ms.ebusdemo.Logger;
import com.ptc.microservices.reactive.IReactiveConnector;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class LimitedComputationResource implements Runnable
{
	ConcurrentLinkedQueue<CompletableFuture<Long>> queue = new ConcurrentLinkedQueue<> ();
	private final int capacity;
	private final long runTimeMSecs;
	private long slotCount = 0;

	public LimitedComputationResource (int capacity, long runTimeMSecs)
	{
		this.runTimeMSecs = runTimeMSecs;
		this.capacity = capacity;
	}

	private synchronized CompletableFuture<Long> newSlot ()
	{
		CompletableFuture<Long> future = new CompletableFuture<> ();
		queue.add (future);
		return future;
	}

	public Single<?> getSlot ()
	{
		return IReactiveConnector.futureToSingle ( () -> newSlot ());
	}

	@Override
	public synchronized void run ()
	{
		queue.stream ().limit (capacity).map (item -> queue.poll ()).forEach (future -> future.complete (slotCount++));
		int size = queue.size ();
		if (size > 0) {
			Logger.log ("Task queue size = " + size);
		}
	}

	public LimitedComputationResource start ()
	{
		Schedulers.computation ().schedulePeriodicallyDirect (this, 0, runTimeMSecs, TimeUnit.MILLISECONDS);
		return this;
	}
}
