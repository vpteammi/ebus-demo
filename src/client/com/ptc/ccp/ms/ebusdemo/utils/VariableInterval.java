package com.ptc.ccp.ms.ebusdemo.utils;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;

public class VariableInterval implements ObservableOnSubscribe<Integer>, Runnable
{
	private final int count;
	private int counter;
	private final int maxDelay;
	private final TimeUnit timeUnit;
	private ObservableEmitter<Integer> emitter;
	private final Random generator;

	private VariableInterval (int count, int maxDelay, TimeUnit timeUnit)
	{
		super ();
		this.count = count;
		this.maxDelay = maxDelay;
		this.timeUnit = timeUnit;
		this.counter = 0;
		this.generator = new Random (count + maxDelay);
	}

	@Override
	public void subscribe (ObservableEmitter<Integer> emitter) throws Exception
	{
		this.emitter = emitter;
		run ();
	}

	@Override
	public void run ()
	{
		if (counter < count) {
			emitter.onNext (counter++);
		}
		if (counter == count) {
			emitter.onComplete ();
		}
		else {
			Schedulers.computation ().scheduleDirect (this, generator.nextInt (maxDelay), timeUnit);
		}
	}
	
	public static Observable<Integer> observable (int count, int maxDelay, TimeUnit timeUnit)
	{
		return Observable.create (new VariableInterval (count, maxDelay, timeUnit));
	}
}
