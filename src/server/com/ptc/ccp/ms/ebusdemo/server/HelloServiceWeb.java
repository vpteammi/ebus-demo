package com.ptc.ccp.ms.ebusdemo.server;

import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI;
import com.ptc.ccp.ms.ebusdemo.utils.ArgUtils;
import com.ptc.microservices.reactive.IReactiveFramework;

public class HelloServiceWeb
{
	public static void main (String[] args)
	{
		IReactiveFramework.getDefault ().createServer (args)
				.registerWebService (HelloAPI.class.getName () + ".web", "/hello",
						new HelloServiceAsync (ArgUtils.getName (args, "Web Service")))
				.start ();
	}
}
