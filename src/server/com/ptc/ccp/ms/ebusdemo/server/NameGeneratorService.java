package com.ptc.ccp.ms.ebusdemo.server;

import java.util.PrimitiveIterator.OfInt;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI.Message;
import com.ptc.ccp.ms.ebusdemo.utils.ArgUtils;
import com.ptc.microservices.reactive.IReactiveFramework;
import com.ptc.microservices.reactive.ReactiveService;
import com.ptc.microservices.reactive.ServiceRequest;

import io.reactivex.Single;

public class NameGeneratorService extends ReactiveService
{
	private final String[] names;
	private final OfInt generator;

	public NameGeneratorService (String[] names)
	{
		this.names = names;
		generator = new Random ().ints (0, names.length).iterator ();
		registerMethod ("hey", this::hey, Message.TYPE);
	}

	// Asynchronous response
	public void hey (ServiceRequest<Message, Message> request)
	{
		request.respond (Single.just (new Message (names[generator.next ()])).delay (1, TimeUnit.SECONDS));
	}

	private final static String[] DEFAULT_FIRST_NAMES = { "Leonardo", "Claude", "Pierre-Auguste", "Pablo", "Henri",
			"Salvador", "Kazimir", "Paul" };
	private final static String[] DEFAULT_LAST_NAMES = { "Da Vinci", "Monet", "Renoir", "Picasso", "Matisse", "Dali",
			"Malevich", "Cezanne" };

	public static void main (String[] args)
	{
		boolean lastName = ArgUtils.hasArg (args, "last");
		IReactiveFramework.getDefault ().createServer (args)
				.registerEventBusService (lastName ? "last-names-service" : "first-names-service",
						new NameGeneratorService (lastName ? DEFAULT_LAST_NAMES : DEFAULT_FIRST_NAMES))
				.start ();
	}
}
