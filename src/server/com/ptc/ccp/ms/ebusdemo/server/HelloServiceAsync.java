package com.ptc.ccp.ms.ebusdemo.server;

import java.util.concurrent.TimeUnit;

import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI;
import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI.Message;
import com.ptc.ccp.ms.ebusdemo.utils.ArgUtils;
import com.ptc.microservices.reactive.ServiceRequest;

import io.reactivex.Single;

public class HelloServiceAsync extends HelloReactiveService
{
	public HelloServiceAsync (String name)
	{
		super (name);
		registerMethod ("heyCB", this::heyCB, Message.TYPE);
	}

	// Asynchronous response (RxJava style)
	public void hey (ServiceRequest<Message, Message> request)
	{
		request.respond (Single.just (buildResponse (request)).delay (5, TimeUnit.SECONDS));
	}

	// Asynchronous response (callback style)
	public void heyCB (ServiceRequest<Message, Message> request)
	{
		Single.just (buildResponse (request)).delay (5, TimeUnit.SECONDS).subscribeOn (scheduler ())
				.subscribe (msg -> request.respond (msg));
	}

	public static void main (String[] args)
	{
		reactiveFramework.createServer (args)
				.registerEventBusService (HelloAPI.class, new HelloServiceAsync (ArgUtils.getName (args, "Limiting Service")))
				.start ();
	}
}
