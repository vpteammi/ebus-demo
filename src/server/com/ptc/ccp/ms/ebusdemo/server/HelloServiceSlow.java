package com.ptc.ccp.ms.ebusdemo.server;

import java.util.concurrent.TimeUnit;

import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI;
import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI.Message;
import com.ptc.ccp.ms.ebusdemo.utils.ArgUtils;
import com.ptc.microservices.reactive.ServiceRequest;

import io.reactivex.Single;

public class HelloServiceSlow extends HelloReactiveService
{
	private boolean fast = true;

	public HelloServiceSlow (String name)
	{
		super (name);
	}

	// Asynchronous response
	public void hey (ServiceRequest<Message, Message> request)
	{
		request.respond (Single.just (buildResponse (request)).delay ((fast = !fast) ? 1 : 5, TimeUnit.SECONDS));
	}

	public static void main (String[] args)
	{
		reactiveFramework.createServer (args)
				.registerEventBusService (HelloAPI.class, new HelloServiceSlow (ArgUtils.getName (args, "Slow Service")))
				.start ();
	}
}
