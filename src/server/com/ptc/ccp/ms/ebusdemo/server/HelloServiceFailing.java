package com.ptc.ccp.ms.ebusdemo.server;

import java.util.concurrent.TimeUnit;

import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI;
import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI.Message;
import com.ptc.ccp.ms.ebusdemo.utils.ArgUtils;
import com.ptc.microservices.ServiceException;
import com.ptc.microservices.reactive.ServiceRequest;

import io.reactivex.Single;

public class HelloServiceFailing extends HelloReactiveService
{
	private boolean success = true;

	public HelloServiceFailing (String name)
	{
		super (name);
	}

	// Asynchronous response (RxJava style)
	public void hey (ServiceRequest<Message, Message> request)
	{
		request.respond ((success = !success) ? Single.just (buildResponse (request))
				: Single.<Message> error (new ServiceException ("Error from " + getSignature ())).delay (1,
						TimeUnit.SECONDS));
	}

	public static void main (String[] args)
	{
		reactiveFramework.createServer (args)
				.registerEventBusService (HelloAPI.class, new HelloServiceFailing (ArgUtils.getName (args, "Failing Service")))
				.start ();
	}
}
