package com.ptc.ccp.ms.ebusdemo.server;

import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI.Message;
import com.ptc.ccp.ms.ebusdemo.utils.ArgUtils;
import com.ptc.microservices.ServiceException;
import com.ptc.microservices.reactive.IReactiveFramework;
import com.ptc.microservices.reactive.ReactiveService;
import com.ptc.microservices.reactive.ServiceRequest;
import com.sm.javax.langx.Strings;

import io.reactivex.Single;

public abstract class HelloReactiveService extends ReactiveService implements ArgUtils
{
	public static IReactiveFramework reactiveFramework = IReactiveFramework.getDefault ();
	
	private final String signature;

	public HelloReactiveService (String name)
	{
		this.signature = Strings.isEmpty (name) ? "" : (" " + name);
		registerMethod ("hey", this::hey, Message.TYPE);
	}

	protected String getSignature ()
	{
		return signature;
	}

	protected Message buildResponse (ServiceRequest<Message, Message> request)
	{
		return buildResponse (request.data ());
	}

	protected Message buildResponse (Message request)
	{
		return buildResponse (request.text);
	}

	protected Message buildResponse (String name)
	{
		return new Message ("Hello " + name + "!" + signature);
	}

	public void hey (ServiceRequest<Message, Message> request)
	{
		request.respond (Single.<Message> error (new ServiceException (
				"Error from " + getSignature () + ": hey (ServiceRequest<Message, Message> request) not implemented")));
	}
}
