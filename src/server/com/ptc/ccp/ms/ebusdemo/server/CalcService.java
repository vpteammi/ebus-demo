package com.ptc.ccp.ms.ebusdemo.server;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.IntStream;

import com.ptc.ccp.ms.ebusdemo.calculator.ICalculator;
import com.ptc.ccp.ms.ebusdemo.calculator.ICalculator.ExprParams;
import com.ptc.ccp.ms.ebusdemo.calculator.ICalculator.ExprResult;
import com.ptc.ccp.ms.ebusdemo.utils.LimitedComputationResource;
import com.ptc.ccp.vertxservices.reactive.VertxReactiveServer;
import com.ptc.microservices.reactive.IReactiveServer;
import com.ptc.microservices.reactive.ReactiveService;
import com.ptc.microservices.reactive.ServiceRequest;
import com.sm.javax.langx.Args;
import com.sm.javax.langx.Strings;

public class CalcService extends ReactiveService
{
	// Simulation of limited computing resource

	private final static long UNIT_EXECUTION_TIME_MSECS = 50;
	private final static int EXECUTION_UNITS = 4;

	private LimitedComputationResource computer = new LimitedComputationResource (EXECUTION_UNITS,
			UNIT_EXECUTION_TIME_MSECS);

	public CalcService (boolean async)
	{
		if (async) {
			registerAsync ();
		}
		else {
			registerSync ();
		}
		computer.start ();
	}

	// --------------------------------------------------------------------------

	private void registerSync ()
	{
		this.registerMethod ("add", this::add, ExprParams.TYPE)
				.registerMethod ("subtract", this::subtract, ExprParams.TYPE)
				.registerMethod ("multiply", this::multiply, ExprParams.TYPE)
				.registerMethod ("divide", this::divide, ExprParams.TYPE);
	}

	private ExprResult calculateSync (ExprParams expr, BiFunction<Integer, Integer, Integer> oper)
	{
		return new ExprResult (oper.apply (expr.getLeft (), expr.getRight ()), Objects.toString (this));
	}

	public ExprResult add (ExprParams expr)
	{
		return calculateSync (expr, (l, r) -> l + r);
	}

	public ExprResult subtract (ExprParams expr)
	{
		return calculateSync (expr, (l, r) -> l - r);
	}

	public ExprResult multiply (ExprParams expr)
	{
		return calculateSync (expr, (l, r) -> l * r);
	}

	public ExprResult divide (ExprParams expr)
	{
		return calculateSync (expr, (l, r) -> l / r);
	}

	// --------------------------------------------------------------------------

	private void registerAsync ()
	{
		this.registerMethod ("add", this::addAsync, ExprParams.TYPE)
				.registerMethod ("subtract", this::subtractAsync, ExprParams.TYPE)
				.registerMethod ("multiply", this::multiplyAsync, ExprParams.TYPE)
				.registerMethod ("divide", this::divideAsync, ExprParams.TYPE);
	}

	private void calculateAsync (ServiceRequest<ExprParams, ExprResult> request, Function<ExprParams, ExprResult> oper)
	{
		computer.getSlot ().subscribeOn (scheduler ())
				.subscribe (ignore -> request.respond (oper.apply (request.getParameters ())));
	}

	public void addAsync (ServiceRequest<ExprParams, ExprResult> request)
	{
		calculateAsync (request, this::add);
	}

	public void subtractAsync (ServiceRequest<ExprParams, ExprResult> request)
	{
		calculateAsync (request, this::subtract);
	}

	public void multiplyAsync (ServiceRequest<ExprParams, ExprResult> request)
	{
		calculateAsync (request, this::multiply);
	}

	public void divideAsync (ServiceRequest<ExprParams, ExprResult> request)
	{
		calculateAsync (request, this::divide);
	}

	// --------------------------------------------------------------------------

	public static void main (String[] args)
	{
		Args mainArgs = Args.parse (args);

		boolean async = Strings.isEmpty (mainArgs.get ("--sync"));
		String[] names = Strings.parseList (Strings.noEmptyOr (mainArgs.get ("--id"), ICalculator.CALC_SERVICE));
		int count = Integer.parseInt (Strings.noEmptyOr (mainArgs.get ("--instances"), "1"));
		int port = Integer.parseInt (Strings.noEmptyOr (mainArgs.get ("--port"), "8080"));
		String webPath = mainArgs.get ("--webpath");
		String webId = mainArgs.get ("--webid");

		IReactiveServer server = VertxReactiveServer.createInstance (args);

		Arrays.stream (names).forEach (name -> IntStream.range (0, count)
				.forEach (idx -> server.registerEventBusService (name, new CalcService (async))));

		if (!Strings.isEmpty (webPath) && !Strings.isEmpty (webId)) {
			server.registerWebService (webId, port, webPath, new CalcService (async));
		}

		server.start ();
	}
}
