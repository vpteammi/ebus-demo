package com.ptc.ccp.ms.ebusdemo.server;

import com.ptc.ccp.ms.ebusdemo.clients.HelloClientTalking;
import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI;
import com.ptc.ccp.ms.ebusdemo.utils.ArgUtils;
import com.sm.javax.langx.Strings;

public class HelloSelfService extends HelloServiceAsync
{
	public HelloSelfService (String name)
	{
		super (name);
	}

	public static void main (String[] args)
	{
		reactiveFramework.createServer (args)
				.registerEventBusService (HelloAPI.class,
						new HelloSelfService (ArgUtils.getName (args, "Self")))
				.startRx ().subscribe (nothing -> HelloClientTalking.main (Strings.EMPTY_ARRAY));
	}
}
