package com.ptc.ccp.ms.ebusdemo.server;

import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI;
import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI.Message;
import com.ptc.ccp.ms.ebusdemo.utils.ArgUtils;

public class HelloServiceSync extends HelloReactiveService
{
	public HelloServiceSync (String name)
	{
		super (name);
		registerMethod ("hey", this::heySync, Message.TYPE);
	}

	// Fast synchronous response
	// Does not block event loop with long result calculation
	public Message heySync (Message msg)
	{
		return buildResponse (msg);
	}

	public static void main (String[] args)
	{
		reactiveFramework.createServer (args)
				.registerEventBusService (HelloAPI.class, new HelloServiceSync (ArgUtils.getName (args, "Sync Service")))
				.start ();
	}
}
