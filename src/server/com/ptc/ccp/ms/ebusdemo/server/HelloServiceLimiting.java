package com.ptc.ccp.ms.ebusdemo.server;

import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI;
import com.ptc.ccp.ms.ebusdemo.hello.HelloAPI.Message;
import com.ptc.ccp.ms.ebusdemo.utils.LimitedComputationResource;
import com.ptc.microservices.reactive.ServiceRequest;
import com.sm.javax.utilx.CommandLineArguments;

public class HelloServiceLimiting extends HelloReactiveService
{
	private final LimitedComputationResource computer;

	public HelloServiceLimiting (String name, int computationUnits, long compuationTimeMSecs)
	{
		super (name);
		computer = new LimitedComputationResource (computationUnits, compuationTimeMSecs).start ();
		registerMethod ("hey", this::hey, Message.TYPE);
	}

	// Asynchronous response competing for ${computationUnits} execution
	// resources available
	// every ${compuationTimeMSecs} milliseconds
	public void hey (ServiceRequest<Message, Message> request)
	{
		request.respond (computer.getSlot ().map (ignore -> buildResponse (request.data ())));
	}

	public static void main (String[] args)
	{
		CommandLineArguments cargs = CommandLineArguments.parse (args);
		String serviceName = cargs.getArgument ("name", "Limiting Service");
		int computationUnits = cargs.getIntArgument ("units", 1);
		int compuationTimeMSecs = cargs.getIntArgument ("duration", 1000);
		
		reactiveFramework.createServer (args).registerEventBusService (HelloAPI.class,
				new HelloServiceLimiting (serviceName, computationUnits, compuationTimeMSecs)).start ();
	}
}
