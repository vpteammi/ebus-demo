package com.ptc.ccp.ms.ebusdemo.calculator;

import java.lang.reflect.Type;
import java.util.Date;

import com.google.common.reflect.TypeToken;
import com.ptc.microservices.ServiceException;
import com.ptc.microservices.reactive.JsonSerializable;
import com.ptc.microservices.reactive.ServiceMessage;

public interface ICalculator
{
	public ExprResult add (ExprParams expr) throws ServiceException, InterruptedException;
	public ExprResult subtract (ExprParams expr) throws ServiceException, InterruptedException;
	public ExprResult multiply (ExprParams expr) throws ServiceException, InterruptedException;
	public ExprResult divide (ExprParams expr) throws ServiceException, InterruptedException;
	
	// ----------------------------------------------------------	
	public static class ExprParams implements JsonSerializable
	{
		private int left;
		private int right;
		
		public ExprParams (int left, int right)
		{
			super ();
			this.left = left;
			this.right = right;
		}
		
		public int getLeft ()
		{
			return left;
		}
		public void setLeft (int left)
		{
			this.left = left;
		}
		public int getRight ()
		{
			return right;
		}
		public void setRight (int right)
		{
			this.right = right;
		}
		@SuppressWarnings ("serial")
		public static final Type TYPE = new TypeToken<ServiceMessage<ExprParams>>() {}.getType ();
	}
	
	public static String startTime = Long.toString (new Date ().getTime ());
	
	public static class ExprResult implements JsonSerializable
	{
		private Date time = new Date ();
		private int value;
		private String serverId;

		public ExprResult (int value, String serverId)
		{
			super ();
			this.value = value;
			this.serverId = serverId + "@" + startTime;
		}

		public Date getTime() {
			return time;
		}

		public String getServerId() {
			return serverId;
		}

		public int getValue ()
		{
			return value;
		}

		public void setValue (int value)
		{
			this.value = value;
		}
		
		@SuppressWarnings ("serial")
		public static final Type TYPE = new TypeToken<ServiceMessage<ExprResult>>() {}.getType ();
	}
	
	public final static String CALC_SERVICE = "ebus-calculator";
	public final static String ADD_SERVICE = "add-calculator";
	public final static String MULT_SERVICE = "mult-calculator";
}
