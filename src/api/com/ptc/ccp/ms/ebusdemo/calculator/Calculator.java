package com.ptc.ccp.ms.ebusdemo.calculator;

import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import com.ptc.microservices.ServiceException;
import com.ptc.microservices.reactive.IReactiveConnector;
import com.ptc.microservices.reactive.ReactiveClient;
import com.ptc.microservices.reactive.ServiceResponse;

import io.reactivex.Single;

public class Calculator extends ReactiveClient<Calculator> implements ICalculator {

	public Calculator(IReactiveConnector connection) {
		super(connection);
	}

	// ----------------------------------------------------------
	@Override
	public ExprResult add(ExprParams expr) throws ServiceException, InterruptedException {
		return synchronize(c -> add(expr, c));
	}

	private CompletableFuture<Integer> toCompletableResult(int o1, int o2,
			BiConsumer<ExprParams, Consumer<ServiceResponse<ExprResult>>> action) {
		return this.<ExprResult>getFuture(c -> action.accept(new ExprParams(o1, o2), c)).thenApply(expr -> {
			System.out.println("serverID=" + expr.getServerId());
			return expr;
		}).thenApply(expr -> expr.getValue());
	}

	private Single<Integer> toRxResult(int o1, int o2,
			BiConsumer<ExprParams, Consumer<ServiceResponse<ExprResult>>> action) {
		return this.<ExprResult>getSingle(c -> action.accept(new ExprParams(o1, o2), c)).map(expr -> {
			System.out.println("serverID=" + expr.getServerId());
			return expr;
		}).map(expr -> expr.getValue());
	}

	public CompletableFuture<Integer> add(int o1, int o2) {
		return toCompletableResult(o1, o2, this::add);
	}

	public Single<Integer> addRx(int o1, int o2) {
		return toRxResult(o1, o2, this::add);
	}

	@Override
	public ExprResult subtract(ExprParams expr) throws ServiceException, InterruptedException {
		return synchronize(c -> subtract(expr, c));
	}

	public CompletableFuture<Integer> subtract(int o1, int o2) {
		return toCompletableResult(o1, o2, this::subtract);
	}

	public Single<Integer> subtractRx(int o1, int o2) {
		return toRxResult(o1, o2, this::subtract);
	}

	@Override
	public ExprResult multiply(ExprParams expr) throws ServiceException, InterruptedException {
		return synchronize(c -> multiply(expr, c));
	}

	public CompletableFuture<Integer> multiply(int o1, int o2) {
		return toCompletableResult(o1, o2, this::multiply);
	}

	public Single<Integer> multiplyRx(int o1, int o2) {
		return toRxResult(o1, o2, this::multiply);
	}

	@Override
	public ExprResult divide(ExprParams expr) throws ServiceException, InterruptedException {
		return synchronize(c -> divide(expr, c));
	}

	public CompletableFuture<Integer> divide(int o1, int o2) {
		return toCompletableResult(o1, o2, this::divide);
	}

	public Single<Integer> divideRx(int o1, int o2) {
		return toRxResult(o1, o2, this::divide);
	}
	
	public void add(ExprParams expr, Consumer<ServiceResponse<ExprResult>> resultHandler) {
		send("add", expr, resultHandler, ExprResult.TYPE /* this::readResponse */);
	}

	public void subtract(ExprParams expr, Consumer<ServiceResponse<ExprResult>> resultHandler) {
		send("subtract", expr, resultHandler, ExprResult.TYPE);
	}

	public void multiply(ExprParams expr, Consumer<ServiceResponse<ExprResult>> resultHandler) {
		send("multiply", expr, resultHandler, ExprResult.TYPE);
	}

	public void divide(ExprParams expr, Consumer<ServiceResponse<ExprResult>> resultHandler) {
		send("divide", expr, resultHandler, ExprResult.TYPE);
	}

}