package com.ptc.ccp.ms.ebusdemo.hello;

import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import com.google.common.reflect.TypeToken;
import com.ptc.microservices.ServiceException;
import com.ptc.microservices.reactive.IReactiveConnector;
import com.ptc.microservices.reactive.JsonSerializable;
import com.ptc.microservices.reactive.ReactiveClient;
import com.ptc.microservices.reactive.ServiceMessage;
import com.ptc.microservices.reactive.ServiceResponse;

import io.reactivex.Single;

public class HelloAPI extends ReactiveClient<HelloAPI>
{

	public HelloAPI (IReactiveConnector connection)
	{
		super (connection);
	}

	public static class Message implements JsonSerializable
	{
		public String text;

		public Message (String text)
		{
			super ();
			this.text = text;
		}

		@SuppressWarnings ("serial")
		public static final Type TYPE = new TypeToken<ServiceMessage<Message>> ()
		{
		}.getType ();
	}

	// callback API
	public void hey (Message msg, Consumer<ServiceResponse<Message>> resultHandler)
	{
		send ("hey", msg, resultHandler, Message.TYPE);
	}

	// synchronized API
	public Message hey (Message msg) throws ServiceException, InterruptedException
	{
		return synchronize (msg, this::hey);
	}

	public String hey (String message) throws ServiceException, InterruptedException
	{
		return hey (new Message (message)).text;
	}

	// API with CompletableFuture
	public CompletableFuture<Message> heyAsync (Message msg)
	{
		return getFuture (msg, this::hey);
	}

	public CompletableFuture<String> heyAsync (String msg)
	{
		return heyAsync (new Message (msg)).thenApply (m -> m.text);
	}

	// RxJava API
	public Single<ServiceResponse<Message>> heyRxResponse (Message msg)
	{
		return getResponseSingle (msg, this::hey);
	}

	public Single<Message> heyRx (Message msg)
	{
		return getSingle (msg, this::hey);
	}

	public Single<String> heyRx (String msg)
	{
		return heyRx (new Message (msg)).map (m -> m.text);
	}
}
